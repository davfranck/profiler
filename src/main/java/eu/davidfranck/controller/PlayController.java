package eu.davidfranck.controller;

import com.fasterxml.jackson.annotation.JsonView;
import eu.davidfranck.dto.GameView;
import eu.davidfranck.dto.PlayerTurnInfos;
import eu.davidfranck.service.PlayerService;
import eu.davidfranck.service.TurnService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/play")
public class PlayController {

    @Autowired
    private PlayerService playerService;

    @RequestMapping(value= "/{uuid}", method = RequestMethod.GET)
    @JsonView(GameView.ReadonlyView.class)
    public @ResponseBody
    PlayerTurnInfos getTurnInfos(@PathVariable String uuid){
        return playerService.getPlayerTurnInfos(uuid);
    }

    @RequestMapping(value= "/{uuid}", method = RequestMethod.POST)
    public void play(@PathVariable String uuid, @RequestBody Map<String, String> formParams) {
        playerService.play(uuid, formParams.get("choices"));
    }
}
