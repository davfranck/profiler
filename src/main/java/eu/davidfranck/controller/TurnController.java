package eu.davidfranck.controller;

import com.fasterxml.jackson.annotation.JsonView;
import eu.davidfranck.domain.Turn;
import eu.davidfranck.dto.GameView;
import eu.davidfranck.service.TurnService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping(value = "/games/{gameUuid}/turns")
public class TurnController {

    private static final Logger LOGGER = LoggerFactory.getLogger(TurnController.class);

    @Autowired
    private TurnService turnService;

    @RequestMapping(value = "/{turnUuid}", method = RequestMethod.GET)
    @JsonView(GameView.AdminView.class)
    public @ResponseBody
    Turn getTurn(@PathVariable String gameUuid, @PathVariable String turnUuid) {
        LOGGER.info("Recup du tour {}", turnUuid);
        Turn turn = turnService.findTurnByUUid(turnUuid);
        turn.checkGameUuid(gameUuid);
        return turn;
    }

    @RequestMapping(value = "/{turnUuid}", method = RequestMethod.POST)
    public void endTurnConfig(@PathVariable String gameUuid, @PathVariable String turnUuid, @RequestBody Map<String, Integer> formParams) {
        Turn turn = turnService.findTurnByUUid(turnUuid);
        turn.checkGameUuid(gameUuid);
        turnService.endTurnConfig(turn, formParams.get("choice1"), formParams.get("choice2"));
    }

    @RequestMapping(value = "/{turnUuid}/resendmail", method = RequestMethod.GET)
    public void resendEmail(@PathVariable String gameUuid, @PathVariable String turnUuid) {
        turnService.resendNewTurnMails(turnUuid, gameUuid);
    }

    @RequestMapping(value = "/{turnUuid}/end", method = RequestMethod.GET)
    @JsonView(GameView.AdminView.class)
    public @ResponseBody
    Turn endTurn(@PathVariable String gameUuid, @PathVariable String turnUuid) {
        Turn turn = turnService.findTurnByUUid(turnUuid);
        turn.checkGameUuid(gameUuid);
        turnService.endTurn(turn);
        return turn;
    }
}
