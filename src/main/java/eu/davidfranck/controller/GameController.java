package eu.davidfranck.controller;

import com.fasterxml.jackson.annotation.JsonView;
import eu.davidfranck.domain.Game;
import eu.davidfranck.dto.GameView;
import eu.davidfranck.dto.ReadOnlyGame;
import eu.davidfranck.service.GameService;
import eu.davidfranck.service.TurnService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping(value = "/games")
public class GameController {

    private static final Logger LOGGER = LoggerFactory.getLogger(GameController.class);

    @Autowired
    private GameService gameService;

    @Autowired
    private TurnService turnService;

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @JsonView(GameView.AdminView.class)
    public @ResponseBody
    Game create(@RequestBody Map<String, String> params) {
        return gameService.create(params.get("mailsJoueurs"), params.get("adminEmail"));
    }

    @RequestMapping(value = "/{uuid}", method = RequestMethod.GET)
    @JsonView(GameView.AdminView.class)
    public @ResponseBody
    Game getGame(@PathVariable String uuid) {
        LOGGER.debug("getGame");
        return gameService.get(uuid);
    }

    @RequestMapping(value = "/readonly/{uuid}", method = RequestMethod.GET)
    @JsonView(GameView.ReadonlyView.class)
    public @ResponseBody
    ReadOnlyGame getReadOnlyGame(@PathVariable String uuid) {
        LOGGER.debug("getReadOnlyGame");
        return gameService.getReadOnly(uuid);
    }

    @RequestMapping(value = "/cancreate", method = RequestMethod.GET)
    public @ResponseBody
    @JsonView(GameView.ReadonlyView.class)
    ReadOnlyGame getLastGame() {
        return gameService.getLastGameReadOnly();
    }

    @RequestMapping(value = "/{gameUuid}/start", method = RequestMethod.GET)
    public @ResponseBody
    Map<String, String> startGame(@PathVariable String gameUuid) {
        LOGGER.info("Demarrage partie {}", gameUuid);
        Game game = gameService.start(gameUuid);
        Map<String, String> mapReturn = new HashMap<>();
        mapReturn.put("gameid", game.getUuid());
        mapReturn.put("turnid", game.getTurns().get(0).getUuid());
        return mapReturn;
    }

    @RequestMapping(value = "/{gameUuid}/newTurn", method = RequestMethod.GET)
    @JsonView(GameView.AdminView.class)
    public @ResponseBody
    Game addNewTurn(@PathVariable String gameUuid) {
        LOGGER.info("Demarrage nouveau tour de la partie {}", gameUuid);
        Game game = gameService.get(gameUuid);
        turnService.addNewTurn(game);
        return game;
    }

    @RequestMapping(value = "/{gameUuid}/end", method = RequestMethod.GET)
    @JsonView(GameView.AdminView.class)
    public @ResponseBody
    Game endGame(@PathVariable String gameUuid) {
        LOGGER.info("Fin de la partie {}", gameUuid);
        return gameService.end(gameUuid);
    }
}
