package eu.davidfranck.dto;

public class GameView {

    public interface ReadonlyView {}
    public interface AdminView extends ReadonlyView {}
}
