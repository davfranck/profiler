package eu.davidfranck.dto;

import com.fasterxml.jackson.annotation.JsonView;
import eu.davidfranck.domain.Game;
import eu.davidfranck.domain.Turn;

import javax.persistence.CascadeType;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ReadOnlyGame {

    private Game game;

    public ReadOnlyGame(Game game) {
        this.game = game;
    }

    @JsonView(GameView.ReadonlyView.class)
    public String getReadOnlyAccessUuid() {
        return game.getReadOnlyAccessUuid();
    }

    @JsonView(GameView.ReadonlyView.class)
    public List<Turn> getTurns() {
        return game.getTurns().stream().filter(turn -> !turn.isNotFinished()).collect(Collectors.toList());
    }

    @JsonView(GameView.ReadonlyView.class)
    public Ranking getRanking() {
        return game.getRanking();
    }

    @JsonView(GameView.ReadonlyView.class)
    public String getStatus(){
        return game.getStatus().name();
    }
}
