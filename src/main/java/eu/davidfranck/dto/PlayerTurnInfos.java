package eu.davidfranck.dto;

import com.fasterxml.jackson.annotation.JsonView;
import eu.davidfranck.domain.TurnPersonCard;
import eu.davidfranck.domain.TurnPhraseCard;

import java.util.SortedSet;

public class PlayerTurnInfos {

    @JsonView(GameView.ReadonlyView.class)
    private SortedSet<TurnPersonCard> personCards;

    @JsonView(GameView.ReadonlyView.class)
    private SortedSet<TurnPhraseCard> phraseCards;

    @JsonView(GameView.ReadonlyView.class)
    private Integer firstPhraseChoice;

    @JsonView(GameView.ReadonlyView.class)
    private Integer secondPhraseChoice;

    public PlayerTurnInfos(SortedSet<TurnPersonCard> personCards, SortedSet<TurnPhraseCard> phraseCards, Integer firstPhraseChoice, Integer secondPhraseChoice) {
        this.personCards = personCards;
        this.phraseCards = phraseCards;
        this.firstPhraseChoice = firstPhraseChoice;
        this.secondPhraseChoice = secondPhraseChoice;
    }

    public SortedSet<TurnPersonCard> getPersonCards() {
        return personCards;
    }

    public SortedSet<TurnPhraseCard> getPhraseCards() {
        return phraseCards;
    }

    public Integer getFirstPhraseChoice() {
        return firstPhraseChoice;
    }

    public Integer getSecondPhraseChoice() {
        return secondPhraseChoice;
    }
}
