package eu.davidfranck.dto;

import com.fasterxml.jackson.annotation.JsonView;
import eu.davidfranck.domain.Game;
import eu.davidfranck.domain.Player;
import eu.davidfranck.domain.PlayerTurnChoice;
import eu.davidfranck.domain.Turn;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Ranking {

    private Game game;

    public Ranking(Game game) {
        this.game = game;
    }

    @JsonView(GameView.ReadonlyView.class)
    public ArrayList<PlayerRank> getPlayerRanks() {
        Stream<PlayerTurnChoice> gamePlayerTurnChoiceStream = game.getTurns()
                .stream()
                .filter(Turn::isFinished)
                .map(turn -> turn.getPlayerTurnChoices())
                .flatMap(Collection::stream);

        Map<Player, List<PlayerTurnChoice>> gameChoicesPerPlayer = gamePlayerTurnChoiceStream.collect(Collectors.groupingBy(PlayerTurnChoice::getPlayer));

        ArrayList<PlayerRank> playerRanks = new ArrayList<>();

        gameChoicesPerPlayer.forEach((player, playerTurnChoices) -> {
            Optional<Integer> gameScore = playerTurnChoices.stream()
                    .filter(playerTurnChoice -> playerTurnChoice.getScore() != null)
                    .map(PlayerTurnChoice::getScore)
                    .reduce((accumulator, score) -> {
                        accumulator += score != null ? score : 0;
                        return accumulator;
                    });
            if (gameScore.isPresent()) {
                playerRanks.add(new PlayerRank(player, gameScore.get()));
            }
        });

        Collections.sort(playerRanks);

        return playerRanks;
    }

    public static class PlayerRank implements Comparable<PlayerRank> {

        @JsonView(GameView.ReadonlyView.class)
        public Integer gameScore;

        @JsonView(GameView.ReadonlyView.class)
        public Player player;

        public PlayerRank(Player player, Integer score) {
            this.gameScore = score;
            this.player = player;
        }

        @Override
        public int compareTo(PlayerRank o) {
            return o.gameScore.compareTo(this.gameScore);
        }
    }
}
