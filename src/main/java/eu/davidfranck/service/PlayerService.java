package eu.davidfranck.service;

import eu.davidfranck.domain.Player;
import eu.davidfranck.domain.PlayerTurnChoice;
import eu.davidfranck.dto.PlayerTurnInfos;
import eu.davidfranck.repository.PlayerRepository;
import eu.davidfranck.repository.PlayerTurnChoiceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PlayerService {
    
    @Autowired
    private PlayerRepository playerRepository;

    @Autowired
    private PlayerTurnChoiceRepository playerTurnChoiceRepository;
    
    public void create(Player player){
        playerRepository.save(player);
    }
    
    public Player findByMail(String mail){
        return playerRepository.findByEmail(mail);
    }


    public PlayerTurnInfos getPlayerTurnInfos(String uuid) {
        PlayerTurnChoice playerTurnChoice = getPlayerTurnChoice(uuid);
        return playerTurnChoice.getTurn().toPlayerTurnInfos();
    }

    public void play(String uuid, String choices) {
        PlayerTurnChoice playerTurnChoice = getPlayerTurnChoice(uuid);
        if(!playerTurnChoice.getTurn().canPlay()){
            throw new IllegalStateException("player can't play, turn has finished");
        }
        playerTurnChoice.setChoices(choices);
        playerTurnChoiceRepository.save(playerTurnChoice);
    }

    private PlayerTurnChoice getPlayerTurnChoice(String uuid) {
        PlayerTurnChoice playerTurnChoice = playerTurnChoiceRepository.findOne(uuid);
        if(playerTurnChoice == null){
            throw new IllegalStateException("Unknown uuid");
        }
        if(playerTurnChoice.getChoices() != null) {
            throw new IllegalStateException("Choice already done");
        }
        return playerTurnChoice;
    }
}
