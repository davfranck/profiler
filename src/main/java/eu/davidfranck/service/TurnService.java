package eu.davidfranck.service;

import eu.davidfranck.domain.*;
import eu.davidfranck.repository.PersonCardRespository;
import eu.davidfranck.repository.PhraseCardRepository;
import eu.davidfranck.repository.PlayerTurnChoiceRepository;
import eu.davidfranck.repository.TurnRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.SortedSet;
import java.util.concurrent.ThreadLocalRandom;

@Service
@Transactional
public class TurnService {

    private static final Logger LOGGER = LoggerFactory.getLogger(TurnService.class);

    @Autowired
    private PersonCardRespository personCardRespository;

    @Autowired
    private PhraseCardRepository phraseCardRepository;

    @Autowired
    private TurnRepository turnRepository;

    @Autowired
    private PlayerTurnChoiceRepository playerTurnChoiceRepository;

    @Autowired
    private MailService mailService;


    private static final int NB_PERSON_TO_FIND = 6;
    private static final int NB_PHRASE_TO_FIND = 2;

    public Turn findTurnByUUid(String uuid) {
        return turnRepository.findByUuid(uuid);
    }

    public void addNewTurn(Game game) {
        LOGGER.debug("add new turn");

        Turn turn = new Turn(game.getTurns().size() + 1);

        addPersonCards(turn);
        addPhrasesCards(turn);
        setPersonToFind(turn);

        game.addNewTurn(turn);

        turnRepository.save(turn);
    }

    public void endTurnConfig(Turn turn, Integer choice1, Integer choice2) {
        LOGGER.debug("end turn config, {}, {}", choice1, choice2);

        turn.setFirstPhraseChoice(choice1);
        turn.setSecondPhraseChoice(choice2);
        turn.setStatus(TurnStatus.STARTED);

        List<PlayerTurnChoice> playerTurnChoices = new ArrayList<>();

        turn.getGame().getPlayers().stream().forEach(player -> {
            PlayerTurnChoice playerTurnChoice = new PlayerTurnChoice(turn, player);
            playerTurnChoices.add(playerTurnChoice);
            turn.addPlayerTurnChoice(playerTurnChoice);
        });
        playerTurnChoices.stream().forEach(playerTurnChoice -> {
            LOGGER.info("sent email to player for current turn");
            mailService.sendNewTurnMessage(playerTurnChoice);
        });
        turnRepository.save(turn);
    }

    public void resendNewTurnMails(String turnUuid, String gameUuid) {
        Turn turn = this.findTurnByUUid(turnUuid);
        if(!turn.getGame().getUuid().equals(gameUuid)){
            throw new IllegalArgumentException("Invalid game uuid");
        }
        turn.getPlayerTurnChoices().stream().forEach(playerTurnChoice -> {
            LOGGER.info("resent email to player for current turn");
            mailService.sendNewTurnMessage(playerTurnChoice);
        });
    }

    private void addPersonCards(Turn turn) {

        long totalPersonCards = personCardRespository.count();

        while (NB_PERSON_TO_FIND != turn.getPersonCards().size()) {
            addOnePersonToTurn(turn, totalPersonCards);
        }
    }

    private void addPhrasesCards(Turn turn) {
        long totalPhraseCards = phraseCardRepository.count();

        while (NB_PHRASE_TO_FIND != turn.getPhraseCards().size()) {
            addOnePhraseToTurn(turn, totalPhraseCards);
        }
    }

    private void setPersonToFind(Turn turn) {
        int randomPersonCardRank = ThreadLocalRandom.current().nextInt(NB_PERSON_TO_FIND) + 1;
        TurnPersonCard randomPersonCard = turn.getPersonCardIdFromRank(randomPersonCardRank);
        turn.setPersonToFind(randomPersonCard.getPersonCard());
    }

    private void addOnePersonToTurn(Turn turn, long totalPersonCards) {
        long randomPersonCardId = ThreadLocalRandom.current().nextLong(totalPersonCards) + 1l;
        if (!anyChoosenPersonCardsHasGivenId(turn.getPersonCards(), randomPersonCardId)) {
            PersonCard personCard = personCardRespository.findOne(randomPersonCardId);
            turn.addPersonCard(personCard);
        }
    }

    private void addOnePhraseToTurn(Turn turn, long totalPhraseCards) {
        long randomPhraseCardId = ThreadLocalRandom.current().nextLong(totalPhraseCards) + 1l;
        if (!anyChoosenPhraseCardsHasGivenId(turn.getPhraseCards(), randomPhraseCardId)) {
            PhraseCard phraseCard = phraseCardRepository.findOne(randomPhraseCardId);
            turn.addPhraseCard(phraseCard);
        }
    }

    private boolean anyChoosenPersonCardsHasGivenId(SortedSet<TurnPersonCard> choosenPersonCards, long randomPersonCardId) {
        return choosenPersonCards.stream().anyMatch((TurnPersonCard turnPersonCard) -> turnPersonCard.getPersonCard().getId() == randomPersonCardId);
    }

    private boolean anyChoosenPhraseCardsHasGivenId(SortedSet<TurnPhraseCard> choosenPhraseCards, long randomPhraseCardId) {
        return choosenPhraseCards.stream().anyMatch((TurnPhraseCard turnPhraseCard) -> turnPhraseCard.getPhraseCard().getId() == randomPhraseCardId);
    }

    public void endTurn(Turn turn) {
        turn.setStatus(TurnStatus.FINISHED);
        turnRepository.save(turn);
    }
}
