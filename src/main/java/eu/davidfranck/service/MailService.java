package eu.davidfranck.service;

import com.fasterxml.jackson.databind.SerializationFeature;
import eu.davidfranck.domain.Game;
import eu.davidfranck.domain.Message;
import eu.davidfranck.domain.PlayerTurnChoice;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.TrustStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import javax.net.ssl.SSLContext;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;

@Service
public class MailService {

    @Value("${mail.send}")
    private String sendEmail;

    private static final Logger LOGGER = LoggerFactory.getLogger(MailService.class);
    private static final String FROM_EMAIL = "contact@davidfranck.eu";
    private static final String FROM_NAME = "David FRANCK";
    private static final String HOST = System.getenv("PROFILER_HOST");

    public void sendNewGameMessage(Game game) {

        Message.Contact from = new Message.Contact(FROM_EMAIL, FROM_NAME);
        Message.Contact to = new Message.Contact(game.getAdminEmail(), "Game Admin");
        String urlAdmin = "Url d'administration de la partie : http://" + HOST + "/#/game/edit/" + game.getUuid();
        String rankingLink = "Lien pour voir le classement : http://" + HOST + "/#/readonly/" + game.getReadOnlyAccessUuid();
        String mailContent = urlAdmin + " - " + rankingLink;
        Message message = new Message(from, to, mailContent, "Nouvelle partie");
        Message.Messages messages = new Message.Messages();
        messages.addMessage(message);

        sendMessages(messages);
    }

    public void sendNewTurnMessage(PlayerTurnChoice playerTurnChoice) {
        Message.Contact from = new Message.Contact(FROM_EMAIL, FROM_NAME);
        Message.Contact to = new Message.Contact(playerTurnChoice.getPlayer().getEmail(), "Player");
        String playLink = "Lien pour jouer : http://" + HOST + "/#/play/" + playerTurnChoice.getUuid();
        String rankingLink = "Lien pour voir le classement : http://" + HOST + "/#/readonly/" + playerTurnChoice.getTurn().getGame().getReadOnlyAccessUuid();
        String mailContent = playLink + " - " + rankingLink;
        Message message = new Message(from, to, mailContent, "Profiler - Nouveau tour");
        Message.Messages messages = new Message.Messages();
        messages.addMessage(message);

        sendMessages(messages);
    }

    private void sendMessages(Message.Messages messages) {
        if (!Boolean.valueOf(sendEmail)) {
            LOGGER.info("Dev mode, no email sent");
            LOGGER.info(messages.toString());
            return;
        }
        RestTemplate restTemplate = getRestTemplate();

        MappingJackson2HttpMessageConverter jsonHttpMessageConverter = new MappingJackson2HttpMessageConverter();
        jsonHttpMessageConverter.getObjectMapper().configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
        restTemplate.getMessageConverters().add(jsonHttpMessageConverter);


        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        String publicKey = System.getenv("PUBLIC_KEY");
        String privateKey = System.getenv("PRIVATE_KEY");

        String plainCreds = publicKey + ":" + privateKey;
        byte[] plainCredsBytes = plainCreds.getBytes();
        byte[] base64CredsBytes = Base64.encodeBase64(plainCredsBytes);
        String base64Creds = new String(base64CredsBytes);

        headers.add("Authorization", "Basic " + base64Creds);

        HttpEntity<Message.Messages> request = new HttpEntity<Message.Messages>(messages, headers);

        try {
            restTemplate.postForObject("https://api.mailjet.com/v3.1/send", request, Object.class);
        } catch (HttpClientErrorException e) {
            LOGGER.error("Mail non envoyé");
            LOGGER.error(e.getResponseBodyAsString());
        }
    }

    // avoid ssl certificate verification (due to proxy man in the middle at work)
    private RestTemplate getRestTemplate() {
        TrustStrategy acceptingTrustStrategy = (X509Certificate[] chain, String authType) -> true;

        SSLContext sslContext = null;
        try {
            sslContext = org.apache.http.ssl.SSLContexts.custom()
                    .loadTrustMaterial(null, acceptingTrustStrategy)
                    .build();
        } catch (NoSuchAlgorithmException e) {
            throw new IllegalStateException();
        } catch (KeyManagementException e) {
            throw new IllegalStateException();
        } catch (KeyStoreException e) {
            throw new IllegalStateException();
        }

        SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(sslContext);

        CloseableHttpClient httpClient = HttpClients.custom()
                .setSSLSocketFactory(csf)
                .build();

        HttpComponentsClientHttpRequestFactory requestFactory =
                new HttpComponentsClientHttpRequestFactory();

        requestFactory.setHttpClient(httpClient);

        return new RestTemplate(requestFactory);
    }
}
