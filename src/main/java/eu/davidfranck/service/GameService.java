package eu.davidfranck.service;

import eu.davidfranck.domain.Game;
import eu.davidfranck.domain.GameStatus;
import eu.davidfranck.domain.Player;
import eu.davidfranck.dto.ReadOnlyGame;
import eu.davidfranck.repository.GameRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.UUID;

@Service
@Transactional
public class GameService {

    @Autowired
    private GameRepository gameRepository;
    
    @Autowired
    private PlayerService playerService;

    @Autowired
    private TurnService turnService;

    @Autowired
    private MailService mailService;


    public void create(Game game) {
        game.setUuid(UUID.randomUUID().toString());
        game.setStatus(GameStatus.CREATED);
        gameRepository.save(game);
    }

    public Game create(String playersMails, String adminEmail) {
        Game game = new Game();
        String[] playersMailsArray = playersMails.split(";");
        for (String playerMail : playersMailsArray) {
            Player player = playerService.findByMail(playerMail);
            if(player != null){
                game.addPlayer(player);
            } else {
                game.addPlayer(new Player(playerMail));
            }
        }
        game.setAdminEmail(adminEmail);
        this.create(game);
        mailService.sendNewGameMessage(game);
        return game;
    }

    public void update(Game game) {
        gameRepository.save(game);
    }

    public Game get(String uuid) {
        Game game = gameRepository.findByUuid(uuid);
        if(game == null) {
            throw new IllegalArgumentException("No game with the given id");
        }
        return game;
    }

    public ReadOnlyGame getReadOnly(String gameReadonlyAccessUuid) {
        Game game = gameRepository.findByReadOnlyAccessUuid(gameReadonlyAccessUuid);
        return game.toReadOnly();
    }

    public ReadOnlyGame getLastGameReadOnly() {
        Game game = gameRepository.findFirst1ByOrderByIdDesc();
        if(game != null){
            return game.toReadOnly();
        }
        return null;
    }

    public Game start(String gameUuid) {
        Game game = this.get(gameUuid);
        turnService.addNewTurn(game);
        game.setStatus(GameStatus.IN_PROGRESS);
        gameRepository.save(game);
        // TODO envoi mail vers /edit de la partie
        return game;
    }

    public Game end(String gameUuid) {
        Game game = this.get(gameUuid);
        game.end();
        return game;
    }
}
