package eu.davidfranck.domain;

import com.fasterxml.jackson.annotation.JsonView;
import eu.davidfranck.dto.GameView;
import eu.davidfranck.dto.PlayerTurnInfos;
import org.hibernate.annotations.SortComparator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.*;

@Entity(name = "Turn")
public class Turn implements Serializable {

    @Id
    @GeneratedValue
    private Long id;

    @NotNull
    @ManyToOne
    private Game game;

    @NotNull
    @JsonView(GameView.ReadonlyView.class)
    private Integer number;

    @NotNull
    @JsonView(GameView.AdminView.class)
    private String uuid;

    @OneToMany(mappedBy = "turn", cascade = CascadeType.ALL, orphanRemoval = true)
    @SortComparator(TurnPersonCardComparator.class)
    @JsonView(GameView.ReadonlyView.class)
    private SortedSet<TurnPersonCard> personCards = new TreeSet<>();

    @OneToMany(mappedBy = "turn", cascade = CascadeType.ALL, orphanRemoval = true)
    @SortComparator(TurnPhraseCardComparator.class)
    @JsonView(GameView.ReadonlyView.class)
    private SortedSet<TurnPhraseCard> phraseCards = new TreeSet<>();

    @NotNull
    @ManyToOne
    @JoinColumn(name = "person_to_find_id",
            foreignKey = @ForeignKey(name = "fk_person_to_find")
    )
    @JsonView(GameView.ReadonlyView.class)
    private PersonCard personToFind;

    @JsonView(GameView.ReadonlyView.class)
    private Integer firstPhraseChoice;

    @JsonView(GameView.ReadonlyView.class)
    private Integer secondPhraseChoice;

    @Enumerated(EnumType.STRING)
    @JsonView(GameView.ReadonlyView.class)
    private TurnStatus status;

    @OneToMany(mappedBy = "turn", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonView(GameView.ReadonlyView.class)
    private List<PlayerTurnChoice> playerTurnChoices = new ArrayList<>();

    public Turn() {
    }

    public Turn(int turnNumber) {
        this.number = turnNumber;
        this.uuid = UUID.randomUUID().toString();
        this.status = TurnStatus.INIT;
    }
    
    public void addPersonCard(PersonCard personCard){
        TurnPersonCard turnPersonCard = new TurnPersonCard(this, personCard, this.personCards.size() + 1);
        this.personCards.add(turnPersonCard);
    }
    
    public void addPhraseCard(PhraseCard phraseCard){
        TurnPhraseCard turnPhraseCard = new TurnPhraseCard(this, phraseCard, this.getPhraseCards().size() + 1);
        this.phraseCards.add(turnPhraseCard);
    }
    
    public void addPlayerTurnChoice(PlayerTurnChoice playerTurnChoice){
        this.playerTurnChoices.add(playerTurnChoice);
    }

    public TurnPersonCard getPersonCardIdFromRank(int randomPersonCardRank) {
        return this.personCards.stream().filter(turnPersonCard -> turnPersonCard.getRank() == randomPersonCardRank).findFirst().get();
    }
    
    public PlayerTurnInfos toPlayerTurnInfos() {
        return new PlayerTurnInfos(this.personCards, this.phraseCards, this.firstPhraseChoice, this.secondPhraseChoice);
    }

    public boolean canPlay() {
        return this.status.equals(TurnStatus.STARTED);
    }

    public void checkGameUuid(String gameUuid) {
        if(!this.getGame().getUuid().equals(gameUuid)){
            throw new IllegalArgumentException("Invalid game uuid");
        }
    }

    public int getRankForPersonId(Long personToFindId) {
        TurnPersonCard turnPersonCardForGivenPersonId = this.personCards.stream().filter(turnPersonCard -> turnPersonCard.getPersonCard().getId().equals(personToFindId)).findFirst().get();
        return turnPersonCardForGivenPersonId.getRank();
    }

    public TurnPersonCard getPersonAtRank(Integer rank) {
        return this.personCards.stream().filter(turnPersonCard -> turnPersonCard.getRank() ==  rank).findFirst().get();
    }

    public boolean isNotFinished() {
        return !this.status.equals(TurnStatus.FINISHED);
    }

    public boolean isFinished() {
        return this.status.equals(TurnStatus.FINISHED);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public SortedSet<TurnPersonCard> getPersonCards() {
        return personCards;
    }

    public void setPersonCards(SortedSet<TurnPersonCard> personCards) {
        this.personCards = personCards;
    }

    public SortedSet<TurnPhraseCard> getPhraseCards() {
        return phraseCards;
    }

    public void setPhraseCards(SortedSet<TurnPhraseCard> phraseCards) {
        this.phraseCards = phraseCards;
    }

    public List<PlayerTurnChoice> getPlayerTurnChoices() {
        return playerTurnChoices;
    }

    public void setPlayerTurnChoices(List<PlayerTurnChoice> playerTurnChoices) {
        this.playerTurnChoices = playerTurnChoices;
    }

    public PersonCard getPersonToFind() {
        return personToFind;
    }

    public void setPersonToFind(PersonCard personToFind) {
        this.personToFind = personToFind;
    }

    public Integer getFirstPhraseChoice() {
        return firstPhraseChoice;
    }

    public void setFirstPhraseChoice(Integer firstPhraseChoice) {
        this.firstPhraseChoice = firstPhraseChoice;
    }

    public Integer getSecondPhraseChoice() {
        return secondPhraseChoice;
    }

    public void setSecondPhraseChoice(Integer secondPhraseChoice) {
        this.secondPhraseChoice = secondPhraseChoice;
    }

    public TurnStatus getStatus() {
        return status;
    }

    public void setStatus(TurnStatus status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Turn turn = (Turn) o;
        return Objects.equals(number, turn.number) &&
                Objects.equals(uuid, turn.uuid);
    }

    @Override
    public int hashCode() {

        return Objects.hash(number, uuid);
    }

    public static class TurnPersonCardComparator implements Comparator<TurnPersonCard> {

        @Override
        public int compare(TurnPersonCard o1, TurnPersonCard o2) {
            return o1.compareTo(o2);
        }
    }
    public static class TurnPhraseCardComparator implements Comparator<TurnPhraseCard> {

        @Override
        public int compare(TurnPhraseCard o1, TurnPhraseCard o2) {
            return o1.compareTo(o2);
        }
    }

}
