package eu.davidfranck.domain;

public enum TurnStatus {
    INIT, STARTED, FINISHED
}
