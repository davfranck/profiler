package eu.davidfranck.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonView;
import eu.davidfranck.dto.GameView;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.io.Serializable;
import java.util.Objects;

@Entity(name = "TurnPhraseCard")
@JsonIgnoreProperties({ "turn" })
public class TurnPhraseCard implements Serializable, Comparable<TurnPhraseCard> {

    @Id
    @ManyToOne
    private Turn turn;

    @Id
    @ManyToOne
    @JsonView(GameView.ReadonlyView.class)
    private PhraseCard phraseCard;

    @JsonView(GameView.ReadonlyView.class)
    private Integer rank;

    public TurnPhraseCard() {
    }

    public TurnPhraseCard(Turn turn, PhraseCard phraseCard, int rank) {
        this.turn = turn;
        this.phraseCard = phraseCard;
        this.rank = rank;
    }

    public Turn getTurn() {
        return turn;
    }

    public void setTurn(Turn turn) {
        this.turn = turn;
    }

    public PhraseCard getPhraseCard() {
        return phraseCard;
    }

    public void setPhraseCard(PhraseCard phraseCard) {
        this.phraseCard = phraseCard;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TurnPhraseCard that = (TurnPhraseCard) o;
        return rank == that.rank &&
                Objects.equals(phraseCard, that.phraseCard);
    }

    @Override
    public int hashCode() {

        return Objects.hash(phraseCard, rank);
    }

    @Override
    public int compareTo(TurnPhraseCard o) {
        return this.rank.compareTo(o.getRank());
    }
}
