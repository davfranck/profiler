package eu.davidfranck.domain;

import com.fasterxml.jackson.annotation.JsonView;
import eu.davidfranck.dto.GameView;
import eu.davidfranck.dto.Ranking;
import eu.davidfranck.dto.ReadOnlyGame;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

@Entity(name = "Game")
public class Game implements Serializable {
    
    @Id
    @GeneratedValue
    private Long id;
    
    @NotNull
    @JsonView(GameView.AdminView.class)
    private String uuid;

    @NotNull
    @JsonView(GameView.ReadonlyView.class)
    private String readOnlyAccessUuid;

    @NotNull
    @JsonView(GameView.AdminView.class)
    private String adminEmail;
    
    @Enumerated(EnumType.STRING)
    @JsonView(GameView.ReadonlyView.class)
    private GameStatus status;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JsonView(GameView.ReadonlyView.class)
    private List<Player> players = new ArrayList<Player>();

    @OneToMany(mappedBy = "game", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonView(GameView.ReadonlyView.class)
    private List<Turn> turns = new ArrayList<>();
    
    public Game(){
        uuid = UUID.randomUUID().toString();
        readOnlyAccessUuid = UUID.randomUUID().toString();
    }

    public void addPlayer(Player player){
        this.players.add(player);
    }

    public void end() {
        this.status = GameStatus.FINISHED;
    }

    public Turn addNewTurn(Turn turn) {
        this.turns.add(turn);
        turn.setGame(this);
        return turn;
    }

    public ReadOnlyGame toReadOnly() {
        return new ReadOnlyGame(this);
    }

    public Turn lastTurn() {
        return this.turns.get(this.turns.size() - 1);
    }

    @JsonView(GameView.ReadonlyView.class)
    public Ranking getRanking(){
        return new Ranking(this);
    }
    
    // Getter setter for Jackson

    public Long getId() {
        return id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getReadOnlyAccessUuid() {
        return readOnlyAccessUuid;
    }

    public void setReadOnlyAccessUuid(String readOnlyAccessUuid) {
        this.readOnlyAccessUuid = readOnlyAccessUuid;
    }

    public String getAdminEmail() {
        return adminEmail;
    }

    public void setAdminEmail(String adminEmail) {
        this.adminEmail = adminEmail;
    }

    public List<Player> getPlayers() {
        return Collections.unmodifiableList(players);
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }

    public GameStatus getStatus() {
        return status;
    }

    public void setStatus(GameStatus status) {
        this.status = status;
    }

    public List<Turn> getTurns() {
        return turns;
    }

    public void setTurns(List<Turn> turns) {
        this.turns = turns;
    }
}
