package eu.davidfranck.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonView;
import eu.davidfranck.dto.GameView;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.io.Serializable;
import java.util.Objects;

@Entity(name = "TurnPersonCard")
public class TurnPersonCard implements Serializable, Comparable<TurnPersonCard> {
    
    @Id
    @ManyToOne
    private Turn turn;
    
    @Id
    @ManyToOne
    @JsonView(GameView.ReadonlyView.class)
    private PersonCard personCard;

    @JsonView(GameView.ReadonlyView.class)
    private int rank;

    public TurnPersonCard() {
    }

    public TurnPersonCard(Turn turn, PersonCard personCard, int rank) {
        this.turn = turn;
        this.personCard = personCard;
        this.rank = rank;
    }

    public Turn getTurn() {
        return turn;
    }

    public void setTurn(Turn turn) {
        this.turn = turn;
    }

    public PersonCard getPersonCard() {
        return personCard;
    }

    public void setPersonCard(PersonCard personCard) {
        this.personCard = personCard;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TurnPersonCard that = (TurnPersonCard) o;
        return rank == that.rank &&
                Objects.equals(personCard, that.personCard);
    }

    @Override
    public int hashCode() {

        return Objects.hash(personCard, rank);
    }

    @Override
    public int compareTo(TurnPersonCard o) {
        return Integer.valueOf(this.rank).compareTo(o.getRank());
    }
}
