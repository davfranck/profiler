package eu.davidfranck.domain;

public enum GameStatus {
    CREATED, IN_PROGRESS, FINISHED
}
