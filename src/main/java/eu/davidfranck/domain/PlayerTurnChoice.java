package eu.davidfranck.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonView;
import eu.davidfranck.dto.GameView;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.*;
import java.util.stream.Collectors;

@Entity
@JsonIgnoreProperties({ "turn" })
public class PlayerTurnChoice {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(PlayerTurnChoice.class);

    @Id
    @NotNull
    @JsonView(GameView.AdminView.class)
    private String uuid;
    
    @NotNull
    @ManyToOne
    private Turn turn;

    @ManyToOne
    @JoinColumn(name = "player_id",
            foreignKey = @ForeignKey(name = "fk_player_turn_choice_player")
    )
    @NotNull
    @JsonView(GameView.ReadonlyView.class)
    private Player player;

    @JsonView(GameView.ReadonlyView.class)
    private String choices;// ranks separated with a space (1 to 6)

    @JsonView(GameView.ReadonlyView.class)
    private Integer score;

    public PlayerTurnChoice() {
        this.uuid = UUID.randomUUID().toString();
    }

    public PlayerTurnChoice(Turn turn, Player player) {
        this();
        this.turn = turn;
        this.player = player;
    }

    public String getUuid() {
        return uuid;
    }
    public Turn getTurn() {
        return turn;
    }

    public void setTurn(Turn turn) {
        this.turn = turn;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public String getChoices() {
        return choices;
    }

    public void setChoices(String choices) {
        if (areChoicesValid(choices)) {
            this.choices = choices;
        } else {
            throw new IllegalArgumentException("Invalid choices");
        }
        computeScore();
    }

    @JsonView(GameView.ReadonlyView.class)
    public List<String> getChoicesAsPersonCards() {
        Set<String> choicesAsSet = getChoicesAsSet(this.choices);
        return choicesAsSet.stream().map(rank -> turn.getPersonAtRank(Integer.valueOf(rank)).getPersonCard().getPerson()).collect(Collectors.toList());
    }

    private boolean areChoicesValid(String choices) {
        Set<String> choicesAsSet = getChoicesAsSet(choices);
        if(choicesAsSet.size() != 6) {
            return false;
        }
        return true;
    }

    private Set<String> getChoicesAsSet(String choices) {
        if(StringUtils.isEmpty(choices)){
            return Collections.emptySet();
        }
        String[] choicesAsArray = choices.split(" ");
        return new LinkedHashSet<>(Arrays.asList(choicesAsArray));
    }

    protected void computeScore() {
        Long personToFindId = this.turn.getPersonToFind().getId();
        LOGGER.debug("Person to find : {}", personToFindId);
        int personToFindRank = this.turn.getRankForPersonId(personToFindId);
        Set<String> choicesAsSet = getChoicesAsSet(this.choices);
        this.score = 0;
        for (String choice : choicesAsSet) {
            if(Long.parseLong(choice) == personToFindRank) {
                LOGGER.debug("The choice {} match", choice);
                break;
            } else {
                LOGGER.debug("The choice {} does not match", choice);
                this.score++;
            }
        }
        if(this.score > 5) {
            throw new IllegalStateException("Bad score :/ personToFindId : " +  personToFindId + " / choicesAsSet: " + choicesAsSet);
        }
        LOGGER.debug("Score : {}", this.score);
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }
}
