package eu.davidfranck.domain;

import com.fasterxml.jackson.annotation.JsonView;
import eu.davidfranck.dto.GameView;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.UUID;

@Entity
public class Player implements Serializable {
    
    @Id
    @GeneratedValue
    private Long id;
    
    @NotNull
    @JsonView(GameView.AdminView.class)
    private String uuid;
    
    @NotNull
    @JsonView(GameView.ReadonlyView.class)
    private String email;

    public Player() {
    }

    public Player(String email) {
        this.uuid = UUID.randomUUID().toString();
        this.email = email;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
