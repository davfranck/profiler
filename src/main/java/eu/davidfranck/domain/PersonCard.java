package eu.davidfranck.domain;

import com.fasterxml.jackson.annotation.JsonView;
import eu.davidfranck.dto.GameView;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Objects;

@Entity
public class PersonCard {

    @Id
    @GeneratedValue
    private Long id;

    @JsonView(GameView.ReadonlyView.class)
    private String person;

    public PersonCard() {
    }

    public PersonCard(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public String getPerson() {
        return person;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PersonCard that = (PersonCard) o;
        return Objects.equals(person, that.person);
    }

    @Override
    public int hashCode() {

        return Objects.hash(person);
    }
}
