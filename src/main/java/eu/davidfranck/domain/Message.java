package eu.davidfranck.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class Message {

    @JsonProperty("From")
    private Contact from;
    @JsonProperty("To")
    private List<Contact> to = new ArrayList<>();
    @JsonProperty("Subject")
    private String subject;
    @JsonProperty("TextPart")
    private String textPart;

    public Message(Contact from, Contact to, String content, String subject) {
        this.from = from;
        this.to.add(to);
        this.textPart = content;
        this.subject = subject;
    }

    public Contact getFrom() {
        return from;
    }

    public List<Contact> getTo() {
        return to;
    }

    public String getSubject() {
        return subject;
    }

    public String getTextPart() {
        return textPart;
    }

    @Override
    public String toString() {
        return "Message{" +
                "from=" + from +
                ", to=" + to +
                ", subject='" + subject + '\'' +
                ", textPart='" + textPart + '\'' +
                '}';
    }

    public static class Contact {

        @JsonProperty("Email")
        public String mail;
        @JsonProperty("Name")
        public String name;

        public Contact(String mail, String name) {
            this.mail = mail;
            this.name = name;
        }

        @Override
        public String toString() {
            return "Contact{" +
                    "mail='" + mail + '\'' +
                    ", name='" + name + '\'' +
                    '}';
        }
    }

    public static class Messages  {

        @JsonProperty("Messages")
        private List<Message> messages = new ArrayList<>();

        public void addMessage(Message message) {
            messages.add(message);
        }

        public List<Message> getMessages() {
            return messages;
        }

        @Override
        public String toString() {
            return "Messages{" +
                    "messages=" + messages +
                    '}';
        }
    }

}
