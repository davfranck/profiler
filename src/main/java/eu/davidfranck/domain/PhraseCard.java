package eu.davidfranck.domain;

import com.fasterxml.jackson.annotation.JsonView;
import eu.davidfranck.dto.GameView;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Objects;

@Entity(name = "PhraseCard")
public class PhraseCard {

    @Id
    @GeneratedValue
    private Long id;

    @JsonView(GameView.ReadonlyView.class)
    private String phrase;

    public Long getId() {
        return id;
    }

    public String getPhrase() {
        return phrase;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PhraseCard that = (PhraseCard) o;
        return Objects.equals(phrase, that.phrase);
    }

    @Override
    public int hashCode() {

        return Objects.hash(phrase);
    }
}
