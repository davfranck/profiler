package eu.davidfranck.repository;

import eu.davidfranck.domain.PlayerTurnChoice;
import org.springframework.data.repository.CrudRepository;

public interface PlayerTurnChoiceRepository extends CrudRepository<PlayerTurnChoice, String> {
}
