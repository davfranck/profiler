package eu.davidfranck.repository;

import eu.davidfranck.domain.Turn;
import org.springframework.data.repository.CrudRepository;

public interface TurnRepository extends CrudRepository<Turn, Long> {
    
    Turn findByUuid(String uuid);
}
