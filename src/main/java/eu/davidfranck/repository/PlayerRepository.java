package eu.davidfranck.repository;

import eu.davidfranck.domain.Player;
import org.springframework.data.repository.CrudRepository;

public interface PlayerRepository extends CrudRepository<Player, Long> {

    Player findByEmail(String email);
}
