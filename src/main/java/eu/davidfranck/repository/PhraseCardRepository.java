package eu.davidfranck.repository;

import eu.davidfranck.domain.PhraseCard;
import org.springframework.data.repository.CrudRepository;

public interface PhraseCardRepository extends CrudRepository<PhraseCard, Long> {
}
