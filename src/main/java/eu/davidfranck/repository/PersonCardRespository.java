package eu.davidfranck.repository;

import eu.davidfranck.domain.PersonCard;
import org.springframework.data.repository.CrudRepository;

public interface PersonCardRespository extends CrudRepository<PersonCard, Long> {
}
