package eu.davidfranck.repository;

import eu.davidfranck.domain.Game;
import eu.davidfranck.domain.GameStatus;
import org.springframework.data.repository.CrudRepository;

import java.util.Collection;

public interface GameRepository extends CrudRepository<Game, Long> {
    
    Game findByUuid(String uuid);
    
    Game findByStatusIn(Collection<GameStatus> statuses);

    Game findByReadOnlyAccessUuid(String uuid);

    Game findFirst1ByOrderByIdDesc();
}
