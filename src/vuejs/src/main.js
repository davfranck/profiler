import Vue from 'vue';
import VueRouter from 'vue-router';
import VueResource from 'vue-resource';
import App from './App.vue';
import { routes } from './routes';
import BootstrapVue from 'bootstrap-vue'
import Vuelidate from 'vuelidate'

Vue.use(BootstrapVue);

Vue.use(VueRouter);

Vue.use(VueResource);

Vue.use(Vuelidate);

Vue.http.options.root = 'http://' + location.hostname + ':8765' + '/';

const router = new VueRouter({
    routes
});

new Vue({
    el: '#app',
    router,
    render: h => h(App)
});