import Home from './components/Home.vue';
import NewGame from './components/NewGame.vue';
import EditGame from './components/EditGame.vue';
import AdminTurn from './components/AdminTurn.vue';
import Play from './components/Play.vue';
import ReadOnlyGame from './components/ReadOnlyGame.vue';

export const routes = [
    { path: '/', component: Home },
    { path: '/newgame', component: NewGame, name: 'newgame' },
    { path: '/game/edit/:id', component: EditGame, name: 'editgame' },
    { path: '/game/:gameid/turn/:turnid', component: AdminTurn, name: 'turn' },
    { path: '/play/:id', component: Play, name: 'play' },
    { path: '/readonly/:id', component: ReadOnlyGame, name: 'readonly' }
];
