package eu.davidfranck.domain;

import org.junit.Test;

import java.util.SortedSet;
import java.util.TreeSet;

import static org.assertj.core.api.Assertions.assertThat;

public class PlayerTurnChoiceTest {

    @Test
    public void computeScore_bad_first_choice_expect_score_0() {
        // given
        PlayerTurnChoice playerTurnChoice = initTurnPersonToFindAtRankOne();
        playerTurnChoice.setChoices("1 2 3 4 5 6");

        // when
        playerTurnChoice.computeScore();

        // then
        assertThat(playerTurnChoice.getScore()).isEqualTo(0);
    }

    @Test
    public void computeScore_bad_second_choice_expect_score_1() {
        // given
        PlayerTurnChoice playerTurnChoice = initTurnPersonToFindAtRankOne();
        playerTurnChoice.setChoices("2 1 3 4 5 6");

        // when
        playerTurnChoice.computeScore();

        // then
        assertThat(playerTurnChoice.getScore()).isEqualTo(1);
    }

    @Test
    public void computeScore_bad_last_choice_expect_score_5() {
        // given
        PlayerTurnChoice playerTurnChoice = initTurnPersonToFindAtRankOne();
        playerTurnChoice.setChoices("2 6 3 4 5 1");

        // when
        playerTurnChoice.computeScore();

        // then
        assertThat(playerTurnChoice.getScore()).isEqualTo(5);
    }

    private PlayerTurnChoice initTurnPersonToFindAtRankOne() {
        PlayerTurnChoice playerTurnChoice = new PlayerTurnChoice();
        Turn turn = new Turn();
        
        PersonCard personToFind = new PersonCard(5L);
        turn.setPersonToFind(personToFind);
        
        SortedSet<TurnPersonCard> personCards = new TreeSet<>();
        personCards.add(new TurnPersonCard(turn, personToFind, 1));
        personCards.add(new TurnPersonCard(turn, new PersonCard(6L), 2));
        personCards.add(new TurnPersonCard(turn, new PersonCard(7L), 3));
        personCards.add(new TurnPersonCard(turn, new PersonCard(8L), 4));
        personCards.add(new TurnPersonCard(turn, new PersonCard(9L), 5));

        turn.setPersonCards(personCards);
        playerTurnChoice.setTurn(turn);
        return playerTurnChoice;
    }
}
