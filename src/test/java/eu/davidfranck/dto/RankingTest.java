package eu.davidfranck.dto;

import eu.davidfranck.domain.*;
import org.junit.Test;

import java.util.ArrayList;

import static org.assertj.core.api.Assertions.assertThat;

public class RankingTest {

    @Test
    public void getPlayerRanks() {
        // given
        Game game = new Game();


        Player playerA = new Player("a");
        Player playerB = new Player("b");

        Turn turn1 = new Turn();
        turn1.setStatus(TurnStatus.FINISHED);
        game.addNewTurn(turn1);

        addPlayerScoreToTurn(turn1, playerA, 1);
        addPlayerScoreToTurn(turn1, playerB, 2);

        Turn turn2 = new Turn();
        turn2.setStatus(TurnStatus.FINISHED);
        game.addNewTurn(turn2);

        addPlayerScoreToTurn(turn2, playerA, 2);
        addPlayerScoreToTurn(turn2, playerB, 3);

        // when
        ArrayList<Ranking.PlayerRank> playerRanks = new Ranking(game).getPlayerRanks();

        // then
        assertThat(playerRanks.get(0).gameScore).isEqualTo(5);
        assertThat(playerRanks.get(0).player.getEmail()).isEqualTo("b");
        assertThat(playerRanks.get(1).gameScore).isEqualTo(3);
        assertThat(playerRanks.get(1).player.getEmail()).isEqualTo("a");
    }

    @Test
    public void getPlayerRanks_empty_game() {
        // given
        Game game = new Game();
        // when
        ArrayList<Ranking.PlayerRank> playerRanks = new Ranking(game).getPlayerRanks();

        // then
        assertThat(playerRanks).isEmpty();
    }

    @Test
    public void getPlayerRanks_empty_turn() {
        // given
        Game game = new Game();

        Turn turn1 = new Turn();
        turn1.setStatus(TurnStatus.FINISHED);
        game.addNewTurn(turn1);
        
        // when
        ArrayList<Ranking.PlayerRank> playerRanks = new Ranking(game).getPlayerRanks();

        // then
        assertThat(playerRanks).isEmpty();
    }

    private void addPlayerScoreToTurn(Turn turn, Player player, int score) {
        PlayerTurnChoice playerATurn1Choice = new PlayerTurnChoice(turn, player);
        turn.addPlayerTurnChoice(playerATurn1Choice);
        playerATurn1Choice.setScore(score);
    }
}
