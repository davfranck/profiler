package eu.davidfranck.service;

import eu.davidfranck.domain.Game;
import eu.davidfranck.domain.Player;
import eu.davidfranck.domain.Turn;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;

import static org.assertj.core.api.Assertions.*;

@SpringBootTest
@RunWith(SpringRunner.class)
@ActiveProfiles(profiles = "test")
@Transactional
public class TurnServiceIntegrationTest {
    
    @Autowired
    private TurnService turnService;
    
    @Autowired
    private GameService gameService;
    
    @Test
    public void addNewTurn(){
        // given
        Game game = new Game();
        game.setAdminEmail("a@a.com");
        game.addPlayer(new Player("test@test.com"));
        game.addPlayer(new Player("test2@test.com"));
        gameService.create(game);
        
        // when
        turnService.addNewTurn(game);
        // then
        assertThat(game.getTurns().size()).isEqualTo(1);
        Turn turn = game.getTurns().get(0);
        assertThat(turn.getPersonCards().size()).isEqualTo(6);
        assertThat(turn.getPhraseCards().size()).isEqualTo(2);
        assertThat(turn.getPersonToFind()).isNotNull();
    }
    
    @Test
    public void addPhrasesChoices() {

        // given
        Game game = new Game();
        game.setAdminEmail("a@a.com");
        gameService.create(game);
        turnService.addNewTurn(game);
        
        // when
        turnService.endTurnConfig(game.getTurns().get(0), 1,2);
        
        // then
        assertThat(game.getTurns().get(0).getFirstPhraseChoice()).isNotNull();
        assertThat(game.getTurns().get(0).getPlayerTurnChoices()).isNotNull();
    }
}
