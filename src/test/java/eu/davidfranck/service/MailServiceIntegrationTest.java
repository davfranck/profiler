package eu.davidfranck.service;

import eu.davidfranck.domain.Game;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;

@SpringBootTest
@RunWith(SpringRunner.class)
@ActiveProfiles(profiles = "test")
@Transactional
public class MailServiceIntegrationTest {
    
    @Autowired
    private MailService mailService;
    
    @Test
    @Ignore // environment variables PUBLIC_KEY and PRIVATE_KEY must be provided
    public void sendNewGameMessage() {
        Game game = new Game();
        game.setAdminEmail("change email");
        mailService.sendNewGameMessage(game);
    }

    
}
