package eu.davidfranck.service;

import eu.davidfranck.domain.Player;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@RunWith(SpringRunner.class)
@ActiveProfiles(profiles = "test")
@Transactional
public class PlayerServiceIntegrationTest {
    
    @Autowired
    private PlayerService playerService;
    
    @Test
    public void testFindByEmail(){
        // given
        playerService.create(new Player("test@test.com"));
        
        //when
        Player player = playerService.findByMail("test@test.com");
        
        //then
        assertThat(player).isNotNull();
        
        // when
        player = playerService.findByMail("inconnu");
        
        // then
        assertThat(player).isNull();
    }
}
