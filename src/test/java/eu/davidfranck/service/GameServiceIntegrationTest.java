package eu.davidfranck.service;

import eu.davidfranck.domain.Game;
import eu.davidfranck.dto.ReadOnlyGame;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@RunWith(SpringRunner.class)
@ActiveProfiles(profiles = "test")
@Transactional
public class GameServiceIntegrationTest {
    
    @Autowired
    private GameService gameService;
    
    @Test
    public void testGetAndSave(){
        Game game = new Game();
        game.setAdminEmail("aa@test.com");
        gameService.create(game);
        assertThat(game.getId()).isPositive();
        assertThat(game.getUuid()).isNotBlank();
        
        Game savedGame = gameService.get(game.getUuid());
        assertThat(savedGame).isEqualTo(game);
    }

    @Test
    public void getReadOnly() {
        // given
        Game game = new Game();
        game.setAdminEmail("aa@test.com");
        gameService.create(game);

        // when
        ReadOnlyGame savedGame = gameService.getReadOnly(game.getReadOnlyAccessUuid());
        // then 
        assertThat(savedGame.getTurns().size()).isEqualTo(0);
    }
    
    @Test
    public void getLastGameReadOnly() {
        assertThat(gameService.getLastGameReadOnly()).isEqualTo(null);
        
        Game game = new Game();
        game.setAdminEmail("a@a.com");
        gameService.create(game);
        assertThat(gameService.getLastGameReadOnly()).isNotNull();
    }
    
    @Test
    public void start() {
        // given
        Game game = new Game();
        game.setAdminEmail("a@a.com");
        gameService.create(game);
        
        // when
        gameService.start(game.getUuid());
        
        // then
        assertThat(game.getTurns()).isNotEmpty();
    }
}
