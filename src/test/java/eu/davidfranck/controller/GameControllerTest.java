package eu.davidfranck.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.davidfranck.domain.Game;
import eu.davidfranck.domain.Player;
import eu.davidfranck.service.GameService;
import eu.davidfranck.service.TurnService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.HashMap;
import java.util.Map;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(GameController.class)
public class GameControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private GameService gameService;

    @MockBean
    private TurnService turnService;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void testSave() throws Exception {
        // Given        
        Map<String, Object> params = new HashMap<>();
        params.put("mailsJoueurs", "1@1.com;2@2.com");
        params.put("adminEmail", "admin@admin.com");

        // When - Then
        String content = this.objectMapper.writeValueAsString(params);
        this.mvc.perform(post("/games/create").contentType(MediaType.APPLICATION_JSON).content(content))
                .andExpect(status().isOk());
    }
}
